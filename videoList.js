var http = require('http');
var memjs = require('memjs');
var mclient = memjs.Client.create();

var videoList = function(safe) {
  return function(request, response) {
    response.set('Content-Type', 'text/json');
    mclient.get('lastUpdated', function(err, val) {
      var touchDate;
      if (err || val === null) {
        // memcached is blank, construct a dummy date
        touchDate = new Date(0);
      } else {
        touchDate = new Date(parseInt(val));
      }
      var now = new Date();
      if ((now.getTime() - touchDate) > 1000 * 60 * 60 * 6) {
        // cache is stale (> 6hr old), refresh cache
        var next = "dummy";
        var videos = [];
        var safevideos = [];

        var scrape = function(nextFn) {
          return function(res) {
            // scrape all videos and move on
            var respText = "";
            res.on("data", function(d) {
              respText += d;
            });
            res.on("end", function() {
              var respObj = JSON.parse(respText);
              var posts = respObj.data.children;
              posts.forEach(function(post) {
                // extract the video URL
                switch(post.data.domain) {
                case "youtube.com":
                  var pos = post.data.url.indexOf("v");
                  if (pos != -1) {
                    var vs = post.data.url.substr(pos+2, 12);
                    videos.push(vs);
                    if (post.data.title.indexOf("TW") == -1) safevideos.push(vs);
                  }
                  break;
                case "youtu.be":
                  var pos = post.data.url.indexOf(".be/");
                  if (pos != -1) {
                    var vs = post.data.url.substr(pos+4, 12);
                    videos.push(vs);
                    if (post.data.title.indexOf("TW") == -1) safevideos.push(vs);
                  }
                  break;
                }
              });
              next = respObj.data.after;
              if (next === null) {
                nextFn();
                return;
              }
              http.get("http://www.thefempire.org/r/intros/.json?after=" + next, scrape(nextFn)).on('error', scrapeError);
            });
          }
        };

        var scrapeError = function(e) {
          // something on the site is broken
          videos = { 'error': e };
        };

        // scrape the videos
        http.get("http://www.thefempire.org/r/intros/.json", scrape(function() {
          // cache the videos list
          mclient.set('videos', JSON.stringify(videos), function(err) {
            console.log("Got error setting videos list: " + err);
          });

          mclient.set('safevideos', JSON.stringify(safevideos), function(err) {
            console.log("Got error setting safe videos list:" + err);
          });

          // set the cache update time
          now = new Date();
          mclient.set('lastUpdated', now.getTime().toString(), function(err) {
            console.log("Got error setting last update time: " + err);
          });

          // return the videos list
          response.send((safe) ? safevideos : videos);
          return;
        })).on('error', scrapeError);
      } else {
        mclient.get((safe) ? 'safevideos' : 'videos', function(err, val) {
          if (err) {
            // memcached fucked up
            response.status(500).send({ 'error': err });
            return;
          }
          response.send(val);
        });
      }
    });
  }
}

module.exports = videoList;