var express = require('express');
var app = express();

// import local libs
var videoList = require('./videoList.js');

// initial config
app.set('port', (process.env.PORT || 5000));

// routing
app.get('/', function(request, response) {
  response.sendfile("index.htm");
});

app.get('/newest', function(request, response) {
  response.set('Set-Cookie','mode=newest');
  response.sendfile("index.htm");
});

app.get('/random', function(request, response) {
  response.set('Set-Cookie','mode=random');
  response.sendfile("index.htm");
});

app.get('/notw', function(request, response) {
  response.set('Set-Cookie','tw=off');
  response.sendfile("index.htm");
});

app.get('/tw', function(request, response) {
  response.set('Set-Cookie','tw=on');
  response.sendfile("index.htm");
});

app.get('/oldest', function(request, response) {
  response.set('Set-Cookie','mode=oldest');
  response.sendfile("index.htm");
});

app.get('/player.js', function(request, response) {
  response.sendfile("player.js");
});

app.get('/brd.png', function(request, response) {
  response.sendfile("brd.png");
});

app.get('/videos', videoList(false));

app.get('/safevideos', videoList(true));

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'));
});


