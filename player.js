/* player.js - intros.watch player */

var player;
var videoCache = [];
var seenVideos = [];

function onYouTubePlayerAPIReady() {
  $.getJSON((readCookie('tw') === 'off') ? '/safevideos' : '/videos', null, function(data, status) {
    if (status === "success") {
      console.log(data);
      videoCache = data;
    } else {
      die();
    }

    var nextVid = selectVid();
    // construct options
    var opts = {
      height: $(window).height().toString(),
      width: '100%',
      videoId: nextVid,
      playerVars: {
        'autoplay': 1, // play the first video when ready
        'iv_load_policy': 3, // disable annotations
        'autohide': 1 // hide player controls
      },
      events: {
        'onStateChange': onPlayerStateChange,
        'onError': onPlayerError
      }
    }

    // set up player
    var player = new YT.Player('player', opts);
  });
}

$(window).resize(function() {
  $("#player").height($(window).height());
});

// dummy error handler
function die() {
  alert("error");
}

// select a video from the cache, pop it & return
function selectVid() {
  if (videoCache.length === 0) {
    // reset the caches
    videoCache = seenVideos;
    seenVideos = [];
  }
  var len = videoCache.length;
  var selection;

  // switch behavior based on cookie-dictated mode
  switch(readCookie("mode")) {
  case "newest":
    selection = videoCache.shift();
    break;
  case "oldest":
    selection = videoCache.pop();
    break;
  case "random":
  default:
    var rand = Math.floor(Math.random() * (len-1));
    selection = videoCache[rand];
    videoCache.splice(rand, 1); // remove selected video
    break;
  }

  seenVideos.push(selection);
  return selection;
}

// when the player's ready
/*
function onPlayerReady(ev) {
  ev.target.playVideo();
}
*/

// when the player changes state
function onPlayerStateChange(ev) {
  if (ev.data === 0) {
    // load next video
    var nextVid = selectVid();
    ev.target.loadVideoById(nextVid, 0, "large");
  }
}

// when the player encounters an error
function onPlayerError(ev) {
  // load next video
  var nextVid = selectVid();
  ev.target.loadVideoById(nextVid, 0, "large");
}

// courtesy of quirksmode.org
// http://www.quirksmode.org/js/cookies.html
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}
